import { renderComponents } from "@/components";
import { CardRow } from "@/components/card-row";
import { PageTitle } from "@/components/page-title";
import { ThemeProvider } from "@/context/theme";
import pages from "@/mock-data/pages";
import { CtaType, PageType, ThemesType } from "@/types";
import { GetStaticPaths, GetStaticProps } from "next";
import Link from "next/link";
import { join } from "path";

type DynamicPagesProps = {
  page: PageType;
  pageNavigation: CtaType[];
};

const DEFAULT_THEME = "default";

type ThemeMappingType = {
  [key: string]: ThemesType;
};

const ThemeMapping: ThemeMappingType = {
  about: "green",
  publications: "red",
  support: "orange",
};

export default function DynamicPages({ page, pageNavigation }: DynamicPagesProps) {
  const theme = ThemeMapping[getSectionFromUrl(page.url)] || DEFAULT_THEME;

  return (
    <ThemeProvider theme={theme}>
      <div className={`theme-${theme}`}>
        <header className="bg-white shadow">
          <div className="px-4 py-6 mx-auto max-w-7xl sm:px-6 lg:px-8">
            <h1 className="mb-4 text-3xl font-bold text-gray-900">
              Section Themes <small className="text-sm font-normal uppercase">({theme})</small>
            </h1>
            <nav>
              <ul className="flex space-x-4">
                {pageNavigation.map((cta) => {
                  return (
                    <li key={cta.title}>
                      <Link href={cta.url} className="text-gray-500 hover:text-gray-700">
                        {cta.title}
                      </Link>
                    </li>
                  );
                })}
              </ul>
            </nav>
          </div>
        </header>
        {renderComponents(page.body)}
      </div>
    </ThemeProvider>
  );
}

export const getStaticPaths: GetStaticPaths = async () => {
  const paths = pages.map((page) => {
    const slug = page.url.split("/").filter((item) => {
      return item !== "";
    });
    console.log(slug);
    return { params: { slug } };
  });
  return {
    paths,
    fallback: false,
  };
};

export const getStaticProps: GetStaticProps = async ({ params }) => {
  let slug = params?.slug || "/";
  if (!Array.isArray(slug)) slug = [slug];

  let path = join("/", ...slug);

  const topLevelPages = pages.filter((page) => {
    return page.url.split("/").length === 2;
  });

  const pageNavigation = topLevelPages.map((page) => {
    return {
      title: page.title,
      url: page.url,
    };
  });

  const page = getPageByPath(path);

  if (!page)
    return {
      notFound: true,
    };

  return {
    props: { page, pageNavigation },
  };
};

function getPageByPath(path: string) {
  return pages.find((page) => page.url === path);
}

function getSectionFromUrl(url: string) {
  const slug = url.split("/").filter((item) => {
    return item !== "";
  });
  return slug[0];
}
