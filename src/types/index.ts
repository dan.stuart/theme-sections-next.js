import { PageTitleProps } from "@/components/page-title";
import { CardRowProps } from "@/components/card-row";

interface ComponentBaseType {
  id?: number;
  component?: string;
}

export type BodyComponent = {
  id?: number;
  component?: string;
} & ( PageTitleProps | CardRowProps )

export type PageType = {
  id: number;
  title: string;
  url: string;
  body: BodyComponent[]
};

export type ThemesType = "default" | "orange" | "blue" | "green" | "red";

export type CtaType = {
  url: string;
  title: string;
}