import { PageType } from "@/types";

const pages: PageType[] = [
  {
    id: 0,
    title: "Home",
    url: "/",
    body: [
      {
        id: 0,
        component: "page-title",
        title: "Homepage",
        leadParagraph: "This is the home page"
      },
      {
        id: 1,
        component: "card-row",
        title: "Card Row",
        cards: [
          {
            id: 0,
            title: "Card 1",
            description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed euismod, nisl vel ultricies lacinia, nisl nisl aliquam nisl, vitae aliquam nisl.",
          },
          {
            id: 1,
            title: "Card 2",
            description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed euismod, nisl vel ultricies lacinia, nisl nisl aliquam nisl, vitae aliquam nisl.",
          },
          {
            id: 2,
            title: "Card 3",
            description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed euismod, nisl vel ultricies lacinia, nisl nisl aliquam nisl, vitae aliquam nisl.",
          },
        ]
      }
    ]
  },
  {
    id: 1,
    title: "About",
    url: "/about",
    body: [
      {
        id: 0,
        component: "page-title",
        title: "About",
        leadParagraph: "This is the about page"
      },
      {
        id: 1,
        component: "card-row",
        title: "Card Row",
        cards: [
          {
            id: 0,
            title: "Card 1",
            description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed euismod, nisl vel ultricies lacinia, nisl nisl aliquam nisl, vitae aliquam nisl.",
          },
          {
            id: 1,
            title: "Card 2",
            description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed euismod, nisl vel ultricies lacinia, nisl nisl aliquam nisl, vitae aliquam nisl.",
          },
          {
            id: 2,
            title: "Card 3",
            description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed euismod, nisl vel ultricies lacinia, nisl nisl aliquam nisl, vitae aliquam nisl.",
          },
        ]
      }
    ]
  },
  {
    id: 1,
    title: "The Company",
    url: "/about/the-company",
    body: [
      {
        id: 0,
        component: "page-title",
        title: "The Company",
        leadParagraph: "This is the company page"
      },
      {
        id: 1,
        component: "card-row",
        title: "Card Row",
        cards: [
          {
            id: 0,
            title: "Card 1",
            description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed euismod, nisl vel ultricies lacinia, nisl nisl aliquam nisl, vitae aliquam nisl.",
          },
          {
            id: 1,
            title: "Card 2",
            description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed euismod, nisl vel ultricies lacinia, nisl nisl aliquam nisl, vitae aliquam nisl.",
          },
          {
            id: 2,
            title: "Card 3",
            description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed euismod, nisl vel ultricies lacinia, nisl nisl aliquam nisl, vitae aliquam nisl.",
          },
        ]
      }
    ]
  },
  {
    id: 1,
    title: "Publications",
    url: "/publications",
    body: [
      {
        id: 0,
        component: "page-title",
        title: "Publications",
        leadParagraph: "This is the publications page"
      },
      {
        id: 1,
        component: "card-row",
        title: "Card Row",
        cards: [
          {
            id: 0,
            title: "Card 1",
            description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed euismod, nisl vel ultricies lacinia, nisl nisl aliquam nisl, vitae aliquam nisl.",
          },
          {
            id: 1,
            title: "Card 2",
            description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed euismod, nisl vel ultricies lacinia, nisl nisl aliquam nisl, vitae aliquam nisl.",
          },
          {
            id: 2,
            title: "Card 3",
            description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed euismod, nisl vel ultricies lacinia, nisl nisl aliquam nisl, vitae aliquam nisl.",
          },
        ]
      }
    ]
  },
  {
    id: 1,
    title: "Support",
    url: "/support",
    body: [
      {
        id: 0,
        component: "page-title",
        title: "Support",
        leadParagraph: "This is the support page"
      },
      {
        id: 1,
        component: "card-row",
        title: "Card Row",
        cards: [
          {
            id: 0,
            title: "Card 1",
            description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed euismod, nisl vel ultricies lacinia, nisl nisl aliquam nisl, vitae aliquam nisl.",
          },
          {
            id: 1,
            title: "Card 2",
            description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed euismod, nisl vel ultricies lacinia, nisl nisl aliquam nisl, vitae aliquam nisl.",
          },
          {
            id: 2,
            title: "Card 3",
            description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed euismod, nisl vel ultricies lacinia, nisl nisl aliquam nisl, vitae aliquam nisl.",
          },
        ]
      }
    ]
  }
];

export default pages;