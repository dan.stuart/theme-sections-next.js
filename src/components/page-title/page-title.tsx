import { PageTitleProps } from "./page-title.types";
import styles from './page-title.module.css'

export const PageTitle = ({ title, leadParagraph }: PageTitleProps) => {
  return (
    <section data-testid="page-title" className={styles['page-title']} >
      <div className="px-4 py-6 mx-auto max-w-7xl sm:px-6 lg:px-8">
        <h1 className="text-6xl font-bold">{title}</h1>
        {leadParagraph && <p className="mt-4 text-2xl font-light">{leadParagraph}</p>}
      </div>
    </section>
  );
};
