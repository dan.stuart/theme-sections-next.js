export interface PageTitleProps {
  title: string;
  leadParagraph?: string;
}
