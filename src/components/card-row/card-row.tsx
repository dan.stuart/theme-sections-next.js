import { CardRowProps } from "./card-row.types";
import styles from "./card-row.module.css";

export const CardRow = ({ title, cards }: CardRowProps) => {
  return (
    <section data-testid="card-row" className="py-12">
      <div className="px-4 py-6 mx-auto max-w-7xl sm:px-6 lg:px-8">
        <h2 className="text-4xl font-bold">{title}</h2>
        <div className="grid grid-cols-1 gap-6 mt-6 sm:grid-cols-2 lg:grid-cols-3">
          {cards.map((card) => {
            return (
              <div key={card.id} className={`p-6 shadow ${styles.card}`}>
                <h3 className="text-2xl font-bold">{card.title}</h3>
                <p className="mt-4 text-lg font-light">{card.description}</p>
              </div>
            );
          })}
        </div>
      </div>
    </section>
  );
};
