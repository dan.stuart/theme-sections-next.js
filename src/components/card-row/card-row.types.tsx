export type CardType = {
  id: number;
  title: string;
  description: string;
}

export interface CardRowProps {
  title: string;
  cards: CardType[];
}
