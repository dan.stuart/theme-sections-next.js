import { BodyComponent } from "@/types";
import { CardRow } from "./card-row";
import { PageTitle } from "./page-title";

export type ComponentKeys = "page-title" | "card-row";

const components: Record<ComponentKeys, any> = {
  "page-title": PageTitle,
  "card-row": CardRow,
};

function renderComponents(components: BodyComponent[]) {
  return components.map((component) => {
    return renderComponent(component);
  });
}

function renderComponent(component: BodyComponent) {
  const Component = components[component.component as ComponentKeys];
  if (!Component) return null;
  return <Component {...component} />;
}

export { renderComponents, renderComponent };
