import { ThemesType } from '@/types'
import React, {
  createContext,
  useContext,
  ReactNode,
  useState,
  useEffect,
} from 'react'

export type ThemeState = {
  theme: ThemesType
}

type ThemeUpdate = {
  setTheme: (theme: ThemesType) => void
}

const ThemeStateContext = createContext<ThemeState | null>(null)
const ThemeUpdateContext = createContext<ThemeUpdate | null>(null)

type Props = {
  children: ReactNode
} & Pick<ThemeState, 'theme'>

export function ThemeProvider({ children, ...props }: Props) {
  const [theme, setTheme] = useState<ThemesType>(
    props.theme || 'blue'
  )

  const state: ThemeState = {
    theme,
  }

  const updateFns: ThemeUpdate = {
    setTheme,
  }

  useEffect(() => {
    setTheme(props.theme)
  }, [props.theme])

  return (
    <ThemeStateContext.Provider value={state}>
      <ThemeUpdateContext.Provider value={updateFns}>
        {children}
      </ThemeUpdateContext.Provider>
    </ThemeStateContext.Provider>
  )
}

function isThemeStateOk(
  state: ThemeState | null
): state is ThemeState {
  return !!state
}

function isThemeUpdateFnsOk(
  updateFns: ThemeUpdate | null
): updateFns is ThemeUpdate {
  return !!updateFns
}

export function useTheme(): [ThemeState, ThemeUpdate] {
  const state = useContext(ThemeStateContext)
  const updateFns = useContext(ThemeUpdateContext)

  if (
    state === undefined ||
    !isThemeStateOk(state) ||
    updateFns === undefined ||
    !isThemeUpdateFnsOk(updateFns)
  ) {
    throw new Error('useTheme must be used within ThemeProvider')
  }

  return [state, updateFns]
}
